
\chapter{Experiments}\label{Experiments}
       \section{Hidden layers}
        First, we made experiments to understand which features would be the best for the hidden layers of MMBMs and what should be the net architecture. We experimented with vgg-line of convolutional networks (CNNs described in~\cite{chatfield2014return}), trained on classification of the “ImageNet” dataset~\cite{deng2009imagenet}. We tried to take features from different layers of different networks and measured the quality of the model. But, due to the great instability of max-margin learning, we couldn't rely on it (if we do, we couldn't determine whether we got poorer quality because of the poorer features or because we were not enough lucky with ICMs starts). And that is why we used different approach to choose the best features. To remind, the connections from the image to hidden layers are in the following form:

        \[ V^1x^1 = \left( \begin{array}{ccc}
            v_{11}^1 & \ldots & v_{1n}^1 \\
            v_{21}^1 & \ldots & v_{2n}^1 \\
              \vdots & \ddots & \vdots \\
            v_{m1}^1 & \ldots & v_{mn}^1
        \end{array} \right) \left( \begin{array}{c} x_1^1 \\ \vdots \\ x_n^1 \end{array} \right)
        \]
        \noindent
        And the rows of $V^1$ are a kind of classifiers that map features set into the corresponding value of the hidden variable. Essentially that representation allowed us to compare different sets of features without max-margin learning, because we were able to pretrain all parts of energy independently (we trained hidden variables classifiers alone and them run the whole MMBM without joint learning).

       \begin{table}[h]
            \begin{center}
                \begin{tabular}{|c|c|c|c|c|c|c|}
                    \hline
                    \multicolumn{7}{|c|}{Results of MMBM1 on Weizmann Horses dataset} \\
                    \hline
                    \multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{2}{c|}{No GC} & \multicolumn{2}{c|}{With GC} &
                                                                                                        \textbf{Linear} \\
                    \hhline{~~----~}
                    \multicolumn{2}{|c|}{} & AP & IoU & AP & IoU & \textbf{classifier} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — conv$_4$} & 89.41 & 78.50 & 89.71 & 79.09 & \textbf{85.81} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — conv$_3$} & 89.31 & 78.33 & 89.58 & 78.84 & \textbf{85.49} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — conv$_2$} & 89.10 & 77.94 & 89.59 & 78.87 & \textbf{84.92} \\
                    \hline
                    \multicolumn{2}{|c|}{HOGs} & 89.03 & 77.83 & 89.63 & 78.95 & \textbf{84.53} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — conv$_5$} & 89.05 & 77.87 & 89.43 & 78.59 & \textbf{84.36} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — conv$_1$} & 88.84 & 77.47 & 89.31 & 78.34 & \textbf{83.98} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — full$_1$} & 88.25 & 76.41 & 89.00 & 77.80 & \textbf{81.57} \\
                    \hline
                    \multicolumn{2}{|c|}{vgg-f — full$_2$} & 87.95 & 75.87 & 88.65 & 77.16 & \textbf{79.96} \\
                    \hline
                \end{tabular}
            \end{center}
            \caption{We report results of MMBM1 without max-margin learning on different metrics and the performance of linear classifiers that predict hidden variables. Here vgg-f is the fast CNN from~\cite{chatfield2014return}, conv$_i$ is i-th convolutional layer from the beginning of the net, full$_i$ is i-th fully-connected layer from the beginning of the net. Each row of the table corresponds to the layer from which we took features for hidden variables. For visible variables we used usual HOGs. GC stands for Graph Cut (we run it on resulting probabilities in order to improve quality). }
            \label{table:exp1}
        \end{table}

        The result of that experiment is presented in table \ref{table:exp1}. HOGs is our baseline row (the performance of usual MMBM). We again proved that features from Convolutional Networks can be used in quite different task than the one on which networks were trained. It is also worth noting that features from the end of the network are too overfitted for the original task (ImageNet classification) and features from the beginning don't have enough generality yet, which means that only features from the middle can be used for our model.

        This pattern has been repeated over and over no matter which architecture we used. That probably means that CNN architecture doesn't influence features quality very much. The results for MMBM2 were approximately the same.

    \section{Visible layer}
        The problem of obtaining visible features was more challenging. It was necessary to extract local features that would be different for every pixel of the image. We couldn't simply use features from the beginning of the CNN because they lacked generality (when we tried that approach, we got approximately 5 percent drop in quality). Another possible way was to change model and consider visible features in the same manner as the hidden ones (obtain the same set of features and train a number of classifiers). But it couldn't work (the same 5\% drop in quality), because of the following reasoning. At the end of the CNN, each variable is some non-linear combination of relevant features for every pixel of the image (produced on the first layers, when locality is still sizable). But for predicting label of the given pixel, such features become inappropriate, because in that case they are essentially just a non-linear combination of relevant features and some random noise (local features for other pixels of the same image). Surely, such features couldn't produce good quality in linear model as was shown in our experiment.

        So, we couldn't use deep CNNs pretrained on classification for our problem. But we could still pretrain our own CNNs that could solve the desired task, i.e. map a local neighborhood of the pixel into its class label (figure \ref{fig:patchcnn}).

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=\linewidth]{images/patchcnn.png}\\
            \end{center}
            \caption{The idea of the patch-to-label CNN. We pass a pixel local neighborhood to the net as an input and get the pixel class as an output.}
            \label{fig:patchcnn}
        \end{figure}

        \noindent
        It was very difficult for us to train big CNNs that map the whole image to something (because our train sets consisted of only few hundreds of images). But we had number-of-pixels-times more training instances in the case of a patch-to-label CNN and that idea actually worked.

        We took features for visible variables from the last layer of this new network (the pixel predicting task was very close to our original one and thus we didn't see any overfitting as in the case of hidden variables). For hidden variables we took features from the forth convolutional layer of vgg-f CNN. In this experiment we used max-margin learning to train the network (not greedy pretraining of all parts as in the previous experiment).

        The results are presented in table \label{table:res-horses} and \label{table:res-birds}.

       \begin{table}[h]
            \begin{center}
                \begin{tabular}{|c|c|c|c|c|c|}
                    \hline
                    \multicolumn{6}{|c|}{MMBM1} \\
                    \hline
                    \multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{2}{c|}{No GC} & \multicolumn{2}{c|}{With GC} \\
                    \hhline{~~----}
                    \multicolumn{2}{|c|}{} & AP & IoU & AP & IoU \\
                    \hline
                    \multicolumn{2}{|c|}{HOGs} & \textbf{89.27} & \textbf{78.27} & 90.09 & 79.83 \\
                    \hline
                    \multicolumn{2}{|c|}{CNN} & 88.49 & 76.84 & \textbf{91.09} & \textbf{81.68} \\
                    \hline
                \end{tabular}
                \quad
                \begin{tabular}{|c|c|c|c|c|c|}
                    \hline
                    \multicolumn{6}{|c|}{MMBM2} \\
                    \hline
                    \multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{2}{c|}{No GC} & \multicolumn{2}{c|}{With GC} \\
                    \hhline{~~----}
                    \multicolumn{2}{|c|}{} & AP & IoU & AP & IoU \\
                    \hline
                    \multicolumn{2}{|c|}{HOGs} & 89.68 & 78.98 & 90.07 & 79.74 \\
                    \hline
                    \multicolumn{2}{|c|}{CNN} & \textbf{91.52} & \textbf{82.35} & \textbf{92.73} & \textbf{84.81} \\
                    \hline
                \end{tabular}
            \end{center}
            \caption{Comparison of our model (CNN) and original model (HOGs) on Weizmann Horses dataset}
            \label{table:res-horses}
        \end{table}

       \begin{table}[h]
            \begin{center}
                \begin{tabular}{|c|c|c|c|c|c|}
                    \hline
                    \multicolumn{6}{|c|}{MMBM1} \\
                    \hline
                    \multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{2}{c|}{No GC} & \multicolumn{2}{c|}{With GC} \\
                    \hhline{~~----}
                    \multicolumn{2}{|c|}{} & AP & IoU & AP & IoU \\
                    \hline
                    \multicolumn{2}{|c|}{HOGs} & 86.31 & 73.26 & 86.92 & 74.29 \\
                    \hline
                    \multicolumn{2}{|c|}{CNN} & \textbf{87.02} & \textbf{74.66} & \textbf{88.23} & \textbf{76.66} \\
                    \hline
                \end{tabular}
                \quad
                \begin{tabular}{|c|c|c|c|c|c|}
                    \hline
                    \multicolumn{6}{|c|}{MMBM2} \\
                    \hline
                    \multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{2}{c|}{No GC} & \multicolumn{2}{c|}{With GC} \\
                    \hhline{~~----}
                    \multicolumn{2}{|c|}{} & AP & IoU & AP & IoU \\
                    \hline
                    \multicolumn{2}{|c|}{HOGs} & 85.32 & 71.81 & 85.89 & 72.73 \\
                    \hline
                    \multicolumn{2}{|c|}{CNN} & \textbf{87.71} & \textbf{75.76} & \textbf{89.26} & \textbf{78.44} \\
                    \hline
                \end{tabular}
            \end{center}
            \caption{Comparison of our model and original model on Caltech-UCSD Birds 200 dataset}
            \label{table:res-birds}
        \end{table}

        \noindent
        We can see, that our model outperforms the original one by about 3 percents. Hopefully, implementation of the joint learning could improve results significantly more.
