
\chapter{Models}\label{Models}

    In this section we briefly describe two types of generative Boltzmann Machines, i.e. Restricted Boltzmann Machines and Shape Boltzmann Machines. Then we introduce Max-Margin Boltzmann Machines for object segmentation.

    \section{Generative Boltzmann Machines}
        Restricted Boltzmann Machine is a Markov Random Field that is used for modeling distribution over a set of visible variables and hidden variables.
        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=0.5\linewidth]{images/rbm.pdf}\\
                \caption{The undirected graph of an RBM with n hidden and m visible variables}
                \label{fig:rbm}
            \end{center}
        \end{figure}

        \noindent
        The distribution over visible and hidden variables is represented as Gibbs distribution with energy in the following form:
        \begin{align*}
            p(y, h) &= \frac{1}{Z}e^{-E(y, h)} = \frac{e^{-E(y, h)}}{\sum_{y, h}e^{-E(y, h)}}\\
            E(y, h) &= -y^TWh - b^Ty - c^Th
        \end{align*}
        \noindent
        As shown in the figure \ref{fig:rbm}, there are no inter-layered connections and thus, all $y$ become independent given $h$ and vice versa:
        \begin{align*}
            p(y|h) &= \prod_{i = 1}^m p(y_i|h);\ p(y_i = 1|h) = \left(\frac{1}{1 + e^{-\sum_{j=1}^m w_{ij}h_j-b_i}}\right) =
            \sigma \left(\sum_{j=1}^m w_{ij}h_j + b_i \right)\\
            p(h|y) &= \prod_{j = 1}^n p(h_j|y);\ p(h_j = 1|y) = \left(\frac{1}{1 + e^{-\sum_{i=1}^n w_{ij}y_i-c_j}}\right) =
            \sigma \left(\sum_{i=1}^n w_{ij}y_i + c_j \right)
        \end{align*}
        \noindent
        Gibbs sampling is usually used for inferring from RBMs and learning can be done by Contrastive Divergence minimization algorithm (?[link]?).

        RBMs can model very complex distributions, but they require a large set of hidden variables and a big number of training samples. In case of object segmentation we usually can't satisfy any of these requirements (i.e. number of labeled images is strictly limited and learning model with big amount of hidden variables is challenging). However it is possible, to make use of spatial structure of the images. For that, Eslami et al.~\cite{eslami2014shape} proposed a particular form of Boltzmann Machines with two layers and shared weights between some of the variables (figure \ref{fig:shapebm}).

        \begin{figure}[h]
            \centering
            \input{sbm.pdf_tex}
            \caption{The structure of ShapeBM}
            \label{fig:shapebm}
        \end{figure}

        Now each neuron in the first hidden layer is connected only to one of the four rectangular patches of the image. And all four patches share the same weights (that means that the whole amount of different weights is approximately four times less). It can be shown that the energy of a model can still be represented in the following form:
        \[ E(y, h^1, h^2, x) = -y^TW^1h^1 - {h^1}^TW^2h^2 - {h^1}^Tc^1 - {h^2}^Tc^2 - y^Tb \]
        Where matrix $W^1$ has kind of a block structure (with the same weights in all four blocks).

        Learning of ShapeBMs consists of greedy pretraining of two layers (we treat them as two independent RBMs) and tuning the whole model with approximate stochastic gradient assent in the likelihood of the full model.
    \section{Max-Margin Boltzmann Machines}
        There are two kinds of MMBMs: MMBM1 which is based on RBM and MMBM2 which is based on ShapeBM. In both cases one more layer is added to the model: features extracted from the original image (HOGs, SIFT descriptors, color histograms and so on). This layer is fully-connected to all other layers of the model (see the figure \ref{fig:mmbms}):

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=0.3\linewidth]{images/mmbm1.png}\hspace{3cm}
                \includegraphics[width=0.3\linewidth]{images/mmbm2.png}\\
                \vspace{2mm}
                \ \ \ \ \ \ MMBM1\hspace{6cm}MMBM2
                \caption{Basic structure of MMBMs}
                \label{fig:mmbms}
            \end{center}
        \end{figure}

        \noindent
        The energy of these models takes the following form:
        \begin{align*}
            E(y, h, x) = &-y^TWh - h^T(V^1x^1 + c) - y^T(V^0x^0 + b) \\
            E(y, h^1, h^2, x) = &-y^TW^1h^1 - {h^1}^TW^2h^2 - {h^1}^T(V^1x^1 + c^1)\ -\\
                                 &-{h^2}^T(V^2x^2 + c^2) - y^T(V^0x^0 + b)
        \end{align*}
        \noindent
        Here $x^0$ is low-level features: SIFT descriptors, color and contour histograms extracted from each superpixel independently. These features are connected to $y$ layer and help to distinguish background from foreground.
        $x^1$ is HOGs extracted from the whole image in the case of MMBM1 and from the four patches in the case of MMBM2. They are connected to the first hidden layer and essentially are the features of object parts.
        And $x^2$ can be thought of as general features that describe the whole object. They are again HOGs extracted from the whole image and they are connected to the second hidden layer.

        Generally speaking, MMBMs differ from RBM and ShapeBM only by bias terms, which are basically the predictions of the corresponding variables, based on the information from the image (i.e. $V^0x^0$ is a segmentation prediction, $V^{1,2}x^{1,2}$ are the predictions of hidden variables).

        Let us see, how these matrices really look like. In the case of visible variables we have different set of features for each variable (because we can extract local features from superpixels and propagate them to all pixels in that superpixel). And that allows us to represent the matrix $V^0$ as a vector (and vector $x^0$ as a matrix):

        In practice, it is possible to obtain a different set of features for each visible variable by extracting local features from superpixels and propagating them to all pixels of superpixel in question. And that allows us to represent the matrix $V^0$ as a vector (and vector $x^0$ as a matrix):

        \[ V^0x^0 =  \left( v_1^0 \ldots v_n^0 \right) \left( \begin{array}{ccc}
            x_{11}^0 & \ldots & x_{m1}^0 \\
            x_{12}^0 & \ldots & x_{m2}^0 \\
              \vdots & \ddots & \vdots \\
            x_{1n}^0 & \ldots & x_{mn}^0
        \end{array} \right)
        \]
        \noindent
        Essentially, $V^0x^0$ is a linear classification model that maps each superpixel to corresponding label ($m$ is the number of superpixels in the image, $n$ is the size of a feature vector, extracted for each superpixel).

        But in the case of hidden layers we can't extract any local features for each variable (because all of them are connected to the whole image). And thus in order to make different predictions over hidden variables we have to train
        $m$ classifiers on the same set of features (here $m$ is the number of hidden variables, $n$ is the size of a features vector, extracted from the image):

        \[ V^1x^1 = \left( \begin{array}{ccc}
            v_{11}^1 & \ldots & v_{1n}^1 \\
            v_{21}^1 & \ldots & v_{2n}^1 \\
              \vdots & \ddots & \vdots \\
            v_{m1}^1 & \ldots & v_{mn}^1
        \end{array} \right) \left( \begin{array}{c} x_1^1 \\ \vdots \\ x_n^1 \end{array} \right)
        \]

        \noindent
        Let us note, that we were a little bit inaccurate in notations here: actually, we have to transpose the product of
        $V$ and $x$ so that it matches the way, these matrices were represented in the energy formula. But, we represented it in a different way in order to emphasize the difference between hidden and visible cases.

        \subsection{Inference and learning}
            Given image, most probable segmentation can be computed as:
            \[ y^* = \argmax_y{p(y|x)} = \argmax_y{\sum_{H}p(y,H|x)}  \]
            But in most cases it is not feasible to compute this maximum analytically or even numerically. In~\cite{yang2014max} it was suggested to use ICM algorithm in order to approximately compute
            \[ \argmax_{y,H}{p(y,H|x)} \]
            Which in turn is an approximation of original sum (we hope, that our distribution becomes highly peaked if not unimodal, which means that there are only few terms that are close to $p(y|x)$ in original sum).
            \begin{center}
                ICM algorithm for inference
            \end{center}
            \hspace{-25pt}\line(1, 0){435}\\
            \hspace{4mm}
            \begin{minipage}[t]{0.4\textwidth}
                \vspace{4mm}
                \begin{enumerate}
                    \item Randomly initialize $h$
                    \item \textbf{while} do not converge \textbf{do}
                    \item \hspace{4mm} $y \leftarrow \max p(y|h, x)$
                    \item \hspace{4mm} $h \leftarrow \max p(y|y, x)$
                    \item \textbf{end while}
                \end{enumerate}
            \end{minipage}\hspace{3cm}
            \begin{minipage}[t]{0.4\textwidth}
                \begin{enumerate}
                    \item Randomly initialize $h^1$
                    \item \textbf{while} do not converge \textbf{do}
                    \item \hspace{4mm} $h^2 \leftarrow \max p(h^2|h^1, x)$
                    \item \hspace{4mm} $y \leftarrow \max p(y|h^1, x)$
                    \item \hspace{4mm} $h^1 \leftarrow \max p(h^1|y, h^2, x)$
                    \item \textbf{end while}
                \end{enumerate}
            \end{minipage}\\

            \hspace{-25pt}\line(1, 0){435}

            \noindent
            We have to note that the only theoretical guarantee here is that the desired probability will increase with every step, but in practice ICM usually can't find real maximum. Furthermore, it is very unstable: starting from different points we can get completely different results.

            With the maximum point, ICM can return a vector of probabilities $p$ (to clarify, maximum is produced from this vector by applying element-wise indicator function $[p_i > 0.5]$). But, instead of taking maximum, it is sensible to run Graph Cut algorithm, using probabilities $p$ as unary potentials (pairwise term is based on the magnitude of the gradients of color channels and is taken from \cite{boykov2001interactive}). This improves the performance of MMBMs by a few percents.

        \subsection{Learning}
            The learning problem is formulated in similar spirit to Structural SVM with latent variables max-margin formulation~\cite{yu2009learning}:

            \begin{align*}
                \text{Find $w$: } &-E(y_i, H^*_i, x_i; w) \ge \max_{y, H}\left[{-E(y, H, x_i, w) + \Delta(y, y_i, H, H_i^*)}\right]\ \forall i \\
                &H_i^* = \argmax_{H}\left[ -E(y_i, H, x; w) \right]
            \end{align*}
            \noindent
            Here $\{x_i, y_i\}$ is train data (images and masks), $w$ is a set of all parameters, $\Delta(y, y_i, H, H_i^*)$ is a margin function. In our experiments and in the original article it is Hamming distance between $\{y, H\}$ and $\{y_i, H_i^*\}$.

            Unfortunately, $H_i^*$ depends on $w$ in some complex way so we can't solve this problem using well-known algorithms for solving max-margin problems (CCCP \cite{yu2009learning} or cutting-plane \cite{joachims2009cutting}). We have to use usual stochastic gradient descent:

            \begin{enumerate}
                \item[]\hspace{-25pt}\line(1, 0){415}
                \item Set t = 0, initialize $w_0, \alpha$ and define $\gamma$
                \item \textbf{while} $t < T$ \textbf{do}
                \item \hspace{4mm} Randomly select a training instance $(x_i, y_i)$
                \item \hspace{4mm} Solve using ICM: $H_i^* \leftarrow \displaystyle\max_H(-E(y_i, H, x_i, w_t))$
                \item \hspace{4mm} Solve using ICM: $\widetilde{y}_i, \widetilde{H}_i \leftarrow \displaystyle\max_{H, y}(-E(y, H, x_i, w_t) + \Delta(y, y_i, H, H_i^*))$
                \item \hspace{4mm} Update $w_{t + 1} = w_t - \alpha(\gamma w_t + \frac{\partial E(\widetilde{y}_i, \widetilde{H}_i, x_i, w)}{\partial w} - \frac{\partial E(y_i, H^*_i, x_i, w)}{\partial w})$
                \item \hspace{4mm} Decrease $\alpha$
                \item \textbf{end while}
                \item[]\hspace{-25pt}\line(1, 0){415}
            \end{enumerate}
            \noindent
            It is worth noting that because of ICM this learning is very unstable. Even when we start from the same point
            $w_0$, we can get completely different results, which makes it very difficult to use this procedure for optimization. But still it works and MMBMs can reach state-of-the-art results.
